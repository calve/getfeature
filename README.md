getfeature.py
=============

A nice script to retreive the current index of feature, and to request a new feature number. Implemented as a RESTFul API.

Setup
-----

getfeature.py depends of bottle

    pip install bottle

Put the current feature index in a ``counter_file`` file

    echo "42" > counter_file

Running
-------

    python main.py


Usage
-----

Get the current last feature

    curl 127.0.0.1:8081/feature

Get a new feature

    curl 127.0.0.1:8081/feature/new
