from bottle import Bottle, run


app = Bottle()
app.f = open("counter_file", 'r')
app.feature = int(app.f.read())


def get_feature():
    return app.feature


def incr_feature():
    with open("counter_file", 'w') as f:
        app.feature += 1
        f.write(str(app.feature))


@app.route('/feature/new')
def new_feature():
    incr_feature()
    tmp = get_feature()
    return "feature-#%04d\n" % tmp


@app.route('/feature')
def last_feature():
    tmp = get_feature()
    return "feature-#%04d\n" % tmp


@app.route('/')
def index():
    return "Nop. Maybe you want `feature` or `feature/new`"

run(app, host='0.0.0.0', port=8081)
